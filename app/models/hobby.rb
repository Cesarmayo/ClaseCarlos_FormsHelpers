# == Schema Information
#
# Table name: hobbies
#
#  id          :integer          not null, primary key
#  name        :string
#  description :string
#  kind        :integer
#  person_id   :integer
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

class Hobby < ApplicationRecord
  belongs_to :person
  enum kind: { indoor: 0, outdoor: 1 }
end
