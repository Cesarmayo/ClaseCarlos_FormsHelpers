# == Schema Information
#
# Table name: people
#
#  id         :integer          not null, primary key
#  name       :string
#  email      :string
#  birtdate   :date
#  gender     :integer
#  dni        :integer
#  customer   :boolean
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Person < ApplicationRecord
  has_many :hobbies
  enum gender: { male: 0, female: 1 }
end
